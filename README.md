# Discrete Element Modelling of Iron Ore Pellets to Include the Effects of Moisture and Fines

This is a digitially recreated version of the Ph.D. Thesis *"Discrete Element Modelling of Iron Ore Pellets to Include the Effects of Moisture and Fines"*. See [here](https://era.ed.ac.uk/handle/1842/8270) for original document.

There are some minor differences between the originally submitted version from 2013 and these differences are mostly additional formatting required for an interactive digitial version and some minor typographical corrections that have been made. Page numbers may vary in the newly generate ddocument.


## Citation
BibTeX Citation:
```
@Manual{Morrissey2022,
  title        = {Discrete Element Modelling of Iron Ore Pellets to Include the Effects of Moisture and Fines},
  address      = {The King's Buildings, Robert Stevenson Road, Edinburgh, EH9 3FB},
  author       = {Morrissey, John P.},
  edition      = {Online},
  organization = {The University of Edinburgh},
  year         = {2022},
  url          = {https://jpmorr.gitlab.io/discrete-element-modelling-of-iron-ore-pellets-to-include-the-effects-of-moisture-and-fines/},
}
```

To cite this document, please use the provided `bibtex` entry or the following text citation:

> **Morrissey, J.P.**, *Discrete Element Modelling of Iron Ore Pellets to Include the Effects of Moisture and Fines*, Ph.D. Thesis (*Online Edition*), Availabe at: [https://jpmorr.gitlab.io/discrete-element-modelling-of-iron-ore-pellets-to-include-the-effects-of-moisture-and-fines](https://jpmorr.gitlab.io/discrete-element-modelling-of-iron-ore-pellets-to-include-the-effects-of-moisture-and-fines). (2022).


## License
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

